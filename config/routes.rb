Rails.application.routes.draw do
  
  get 'places/check'
  get 'places/category'
   resources :places do 
   	resources :comments
   	resources :categories
   end

  root 'places#index'
  root 'places#new'
  root 'places#check'
  root 'places#submit'
 
  root to: 'session#index'
  root to: 'login#index'

resources :places do
    collection do
   get 'category', to: "places#category"


    end
  end

resources :login do
    collection do
      get 'signup', to: "login#new"
      get 'login', to: "login#index"
      post 'login', to: "login#login"
      get 'dashboard', to: "login#dashboard"
      get 'approve',to: "login#approve"
      get 'decline',to: "login#decline"
      get 'deniedboard',to: "login#denied"
      get 'acceptboard',to: "login#accept"
      post 'create',to: "login#show"


    end
  end

   resources :session do
    collection do
      get 'signup', to: "session#new"
      get 'login', to: "session#index"
      post 'login', to: "session#login"
      get 'logout', to: "session#logout"
      get 'dashboard', to: "session#dashboard"
    end
  end

 end


